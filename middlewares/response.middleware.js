const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query

  if (!res.err) {
    const data = res.data;
    res.status(200).json(data);
  } else {
    if (res.err.name === "Validation error!") {
      const error = res.err;

      res
        .status(400)
        .json({ error: true, message: error.message || "Validation error!" });
    } else if (
      res.err.name === "Creation error!" ||
      res.err.name === "Updating error!"
    ) {
      const error = res.err;

      res
        .status(400)
        .json({ error: true, message: error.message || "Can't set data!" });
    } else if (res.err.name === "Can't get data!") {
      const error = res.err;

      res
        .status(404)
        .json({ error: true, message: error.message || "Data not found" });
    } else {
      res.status(500).json({ error: true, message: "Unusual error!" });
    }
  }

  next();
};

exports.responseMiddleware = responseMiddleware;
