const { fighter } = require("../models/fighter");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  let requiredFields = Object.keys(fighter);
  requiredFields.splice(requiredFields.indexOf("id"), 1);

  const dataForValidate = req.body;
  const addedProps = Object.keys(dataForValidate);

  if (!dataForValidate.health) {
    dataForValidate.health = 100;
  }

  let validatedData = {};

  try {
    isObject(dataForValidate);
    isNotEmptyObject(dataForValidate);
    isEqualArrays(requiredFields, addedProps);
    requiredFields.forEach((prop) => {
      let val = dataForValidate[prop];

      isNotEmpty(val);
      if (prop === "name") {
        val = val.trim();
        isString(val);
      }

      // if (prop === "defense" || prop === "power") {
      //   val = Number(val);
      //   minSize(val, prop, 1);
      //   maxSize(val, prop, 10);
      // }

      if (prop === "defense") {
        val = Number(val);
        minSize(val, prop, 1);
        maxSize(val, prop, 10);
      }

      // if (prop === "health") {
      //   val = Number(val);
      //   minSize(val, prop, 1);
      //   maxSize(val, prop, 100);
      // }

      validatedData[prop] = val;
    });
    req.body = validatedData;
  } catch (err) {
    err.name = "Validation error!";
    res.err = err;
  }

  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update

  let requiredFields = Object.keys(fighter);
  requiredFields.splice(requiredFields.indexOf("id"), 1);

  const dataForValidate = req.body;
  const addedProps = Object.keys(dataForValidate);

  let propsToUpdate = [];

  addedProps.forEach((prop) => {
    if (requiredFields.includes(prop)) {
      propsToUpdate.push(prop);
    }
  });

  let validatedData = {};

  try {
    isObject(dataForValidate);
    isNotEmptyObject(dataForValidate);
    // isEqualArrays(requiredFields, addedProps);
    isValidArray(requiredFields, addedProps);
    propsToUpdate.forEach((prop) => {
      let val = dataForValidate[prop];

      isNotEmpty(val);
      if (prop === "name") {
        val = val.trim();
        isString(val);
      }

      if (prop === "defense" || prop === "power") {
        val = Number(val);
        minSize(val, prop, 1);
        maxSize(val, prop, 10);
      }

      if (prop === "defense") {
        val = Number(val);
        minSize(val, prop, 1);
        maxSize(val, prop, 10);
      }

      // if (prop === "health") {
      //   val = Number(val);
      //   minSize(val, prop, 1);
      //   maxSize(val, prop, 100);
      // }

      validatedData[prop] = val;
    });
    req.body = validatedData;
  } catch (err) {
    err.name = "Validation error!";
    res.err = err;
  }

  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

// All validation functions can and must be moved to separated file
//OR better to use some validation libraries
// Created it just for hometask, like all custom validation

function isObject(data) {
  const typeOfData = typeof data;
  if (typeOfData === "object") {
    return data;
  } else {
    throw new Error("Invalid data");
  }
}

function isNotEmptyObject(obj) {
  var size = 0,
    key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }

  if (size > 0) {
    return obj;
  } else {
    throw new Error("Invalid data");
  }
}

function isEqualArrays(arr, arr2) {
  if (arr.length != arr2.length) {
    throw new Error("Invalid data!");
  }

  var on = 0;

  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr2.length; j++) {
      if (arr[i] === arr2[j]) {
        on++;
        break;
      }
    }
  }

  if (on == arr.length) {
    return true;
  } else {
    throw new Error("Invalid data!");
  }
}

function isValidArray(arr, arr2) {
  arr2.forEach((val) => {
    console.log(val);
    if (!arr.includes(val)) {
      throw new Error("Invalid data!");
    }
  });
  return true;
}

function isString(data) {
  const typeOfData = typeof data;
  if (typeOfData === "string") {
    return data;
  } else {
    throw new Error("Invalid format");
  }
}

function isNotEmpty(data) {
  const value = data;
  if (value && value !== "") {
    return data;
  } else {
    throw new Error("There are some empty field!");
  }
}

function minSize(data, prop, minVal) {
  const val = data;

  if (val >= minVal) {
    return data;
  } else {
    prop = prop.charAt(0).toUpperCase() + prop.slice(1);
    throw new Error(prop + " is too low!");
  }
}

function maxSize(data, prop, maxVal) {
  const val = data;
  if (val <= maxVal) {
    return data;
  } else {
    prop = prop.charAt(0).toUpperCase() + prop.slice(1);
    throw new Error(prop + " is too high!");
  }
}
