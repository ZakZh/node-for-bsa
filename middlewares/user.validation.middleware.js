const { user } = require("../models/user");

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation

  let requiredFields = Object.keys(user);
  requiredFields.splice(requiredFields.indexOf("id"), 1);

  const dataForValidate = req.body;
  const addedProps = Object.keys(dataForValidate);
  let validatedData = {};

  try {
    isObject(dataForValidate);
    isNotEmptyObject(dataForValidate);
    isEqualArrays(requiredFields, addedProps);

    // if (addedProps.includes("id")) {
    //   throw new Error("Invalid data!");
    // }

    requiredFields.forEach((prop) => {
      let val = dataForValidate[prop];

      isString(val);
      val = val.trim();
      isNotEmpty(val);

      if (prop === "password") {
        minSize(val, prop, 3);
      }

      if (prop === "email") {
        isEmail(val, ["gmail.com"]);
      }

      if (prop === "phoneNumber") {
        const uaPhoneRegEx = /^\+?3?8?(0\d{2}\d{3}\d{2}\d{2})$/;
        isCorrectPhoneNumber(val, uaPhoneRegEx);
      }

      validatedData[prop] = val;
    });
    req.body = validatedData;
  } catch (err) {
    err.name = "Validation error!";
    res.err = err;
  }

  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  let requiredFields = Object.keys(user);
  requiredFields.splice(requiredFields.indexOf("id"), 1);

  const dataForValidate = req.body;
  const addedProps = Object.keys(dataForValidate);

  let propsToUpdate = [];

  addedProps.forEach((prop) => {
    if (requiredFields.includes(prop)) {
      propsToUpdate.push(prop);
    }
  });

  let validatedData = {};

  try {
    isObject(dataForValidate);
    isNotEmptyObject(dataForValidate);
    // isEqualArrays(requiredFields, addedProps);
    isValidArray(requiredFields, addedProps);
    propsToUpdate.forEach((prop) => {
      let val = dataForValidate[prop];

      isString(val);
      val = val.trim();
      isNotEmpty(val);

      if (prop === "password") {
        minSize(val, prop, 3);
      }

      if (prop === "email") {
        isEmail(val, ["gmail.com"]);
      }

      if (prop === "phoneNumber") {
        const uaPhoneRegEx = /^\+?3?8?(0\d{2}\d{3}\d{2}\d{2})$/;
        isCorrectPhoneNumber(val, uaPhoneRegEx);
      }

      validatedData[prop] = val;
    });
    req.body = validatedData;
  } catch (err) {
    err.name = "Validation error!";
    res.err = err;
  }

  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

// All validation functions can and must be moved to separated file
//OR better to use some validation libraries
// Created it just for hometask, like all custom validation

function isObject(data) {
  const typeOfData = typeof data;
  if (typeOfData === "object") {
    return data;
  } else {
    throw new Error("Invalid data");
  }
}

function isNotEmptyObject(obj) {
  var size = 0,
    key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }

  if (size > 0) {
    return obj;
  } else {
    throw new Error("Invalid data");
  }
}

function isEqualArrays(arr, arr2) {
  if (arr.length != arr2.length) {
    throw new Error("Invalid data!");
  }

  var on = 0;

  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr2.length; j++) {
      if (arr[i] === arr2[j]) {
        on++;
        break;
      }
    }
  }

  if (on == arr.length) {
    return true;
  } else {
    throw new Error("Invalid data!");
  }
}

function isValidArray(arr, arr2) {
  arr2.forEach((val) => {
    console.log(val);
    if (!arr.includes(val)) {
      throw new Error("Invalid data!");
    }
  });
  return true;
}

function isString(data) {
  const typeOfData = typeof data;
  if (typeOfData === "string") {
    return data;
  } else {
    throw new Error("Invalid format");
  }
}

function isNotEmpty(data) {
  const value = data;
  if (value && value !== "") {
    return data;
  } else {
    throw new Error("There are some empty field!");
  }
}

function minSize(data, prop, minVal) {
  const val = data;
  if (val.length >= minVal) {
    return data;
  } else {
    prop = prop.charAt(0).toUpperCase() + prop.slice(1);
    throw new Error(prop + " is too short!");
  }
}

function isEmail(data, acceptableEmails) {
  if (Array.isArray(acceptableEmails) && acceptableEmails.length < 1) {
    throw new Error("Available email domains needed!");
  }

  const emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (emailRegExp.test(data)) {
    emailDomain = data.split("@")[1];

    if (
      Array.isArray(acceptableEmails) &&
      acceptableEmails.includes(emailDomain)
    ) {
      return data;
    } else if (acceptableEmails === emailDomain) {
    } else {
      throw new Error("Unacceptable email domain!");
    }
  } else {
    throw new Error("Unacceptable email!");
  }
}

function isCorrectPhoneNumber(phone, regEx) {
  const phoneRegEx = regEx;
  phone = phone.trim();

  if (phoneRegEx.test(phone)) {
    return phone;
  } else {
    throw new Error("Unacceptable phone number!");
  }
}
