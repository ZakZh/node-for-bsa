const { user } = require("../models/user");
const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user

  search(search) {
    const item = UserRepository.getOne(search);

    if (!item) {
      return null;
    }
    return item;
  }

  getAllUsers() {
    const items = UserRepository.getAll();
    if (!items || items.length === 0) {
      throw new Error("Users not found");
    }
    return items;
  }

  getUser(id) {
    const userData = this.search({ id });

    if (!userData) {
      throw new Error("User not found");
    }

    return userData;
  }

  createUser(data) {
    let requiredFields = Object.keys(user);
    requiredFields.splice(requiredFields.indexOf("id"), 1);
    const userEmail = data.email;
    const userPhone = data.phoneNumber;

    if (
      this.search((x) => x.email === userEmail) ||
      this.search((x) => x.phoneNumber === userPhone)
    ) {
      throw new Error("User is already exist!");
    }

    let userData = {};

    requiredFields.forEach((val) => {
      userData[val] = data[val];
    });

    userData = UserRepository.create(userData);

    return userData;
  }

  updateUserData(id, data) {
    const userId = id;

    let requiredFields = Object.keys(user);
    requiredFields.splice(requiredFields.indexOf("id"), 1);

    let updatedData = {};

    if (!this.search({ id })) {
      throw new Error("User not found");
    }

    requiredFields.forEach((val) => {
      if (data[val]) {
        updatedData[val] = data[val];
      }
    });

    updatedData = UserRepository.update(userId, updatedData);

    return updatedData;
  }

  deleteUser(id) {
    const userId = id;
    let deletedData;
    let data = this.search({ id });
    if (data == null) {
      throw new Error("User not found");
    }

    deletedData = UserRepository.delete(userId);

    return deletedData;
  }
}

module.exports = new UserService();
