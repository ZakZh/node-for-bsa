const { FighterRepository } = require("../repositories/fighterRepository");

const { fighter } = require("../models/fighter");

class FighterService {
  // TODO: Implement methods to work with fighters

  search(search) {
    const item = FighterRepository.getOne(search);

    if (!item) {
      return null;
    }
    return item;
  }

  getAllFighters() {
    const items = FighterRepository.getAll();
    if (!items || items.length === 0) {
      throw new Error("Fighters not found");
    }
    return items;
  }

  getFighter(id) {
    const fighterData = this.search({ id });
    if (!fighterData) {
      throw new Error("Fighter not found");
    }

    return fighterData;
  }

  createFighter(data) {
    let requiredFields = Object.keys(fighter);
    requiredFields.splice(requiredFields.indexOf("id"), 1);

    let fighterData = {};

    requiredFields.forEach((val) => {
      fighterData[val] = data[val];
    });

    fighterData = FighterRepository.create(fighterData);

    return fighterData;
  }

  updateFighterData(id, data) {
    const fighterId = id;

    let requiredFields = Object.keys(fighter);
    requiredFields.splice(requiredFields.indexOf("id"), 1);

    let updatedData = {};

    if (!this.search({ id })) {
      throw new Error("Fighter not found");
    }

    requiredFields.forEach((val) => {
      if (data[val]) {
        updatedData[val] = data[val];
      }
    });

    updatedData = FighterRepository.update(fighterId, updatedData);

    return updatedData;
  }

  deleteFighter(id) {
    const fighterId = id;
    let deletedData;
    let data = this.search({ id });
    if (data == null) {
      throw new Error("Fighter not found");
    }

    deletedData = FighterRepository.delete(fighterId);

    return deletedData;
  }
}

module.exports = new FighterService();
