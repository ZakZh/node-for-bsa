const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user
router
  .route("/")
  .get((req, res, next) => {
    try {
      const data = UserService.getAllUsers();
      res.data = data;
    } catch (err) {
      err.name = "Can't get data!";
      res.err = err;
    }
    next();
  }, responseMiddleware)
  .post(
    createUserValid,
    (req, res, next) => {
      if (!res.err) {
        try {
          let data = req.body;
          data = UserService.createUser(data);
          res.data = data;
        } catch (err) {
          err.name = "Creation error!";
          res.err = err;
        }
      }
      next();
    },
    responseMiddleware
  );

router
  .route("/:id")
  .get((req, res, next) => {
    try {
      let userId = req.params.id;
      const data = UserService.getUser(userId);

      res.data = data;
    } catch (err) {
      err.name = "Can't get data!";
      res.err = err;
    }
    next();
  }, responseMiddleware)
  .put(
    updateUserValid,
    (req, res, next) => {
      if (!res.err) {
        try {
          const userId = req.params.id;
          let data = req.body;
          data = UserService.updateUserData(userId, data);
          res.data = data;
        } catch (err) {
          err.name = "Updating error!";
          res.err = err;
        }
      }
      next();
    },
    responseMiddleware
  )
  .delete((req, res, next) => {
    try {
      const userId = req.params.id;
      let data = UserService.deleteUser(userId);

      res.status(200).json(data[0]);
    } catch (err) {
      res.err = err;
    }
    next();
  });

module.exports = router;
