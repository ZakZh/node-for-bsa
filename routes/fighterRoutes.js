const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");
const fighterService = require("../services/fighterService");

const router = Router();

// TODO: Implement route controllers for fighter

router
  .route("/")
  .get((req, res, next) => {
    try {
      const data = FighterService.getAllFighters();
      res.data = data;
    } catch (err) {
      err.name = "Can't get data!";
      res.err = err;
    }
    next();
  }, responseMiddleware)
  .post(
    createFighterValid,
    (req, res, next) => {
      if (!res.err) {
        try {
          let data = req.body;
          data = FighterService.createFighter(data);
          res.data = data;
        } catch (err) {
          res.err = err;
        }
      }
      next();
    },
    responseMiddleware
  );

router
  .route("/:id")
  .get((req, res, next) => {
    try {
      let fighterId = req.params.id;
      const data = FighterService.getFighter(fighterId);

      res.data = data;
    } catch (err) {
      err.name = "Can't get data!";
      res.err = err;
    }
    next();
  }, responseMiddleware)
  .put(
    updateFighterValid,
    (req, res, next) => {
      if (!res.err) {
        try {
          const fighterId = req.params.id;
          let data = req.body;
          data = FighterService.updateFighterData(fighterId, data);
          res.data = data;
        } catch (err) {
          res.err = err;
        }
      }
      next();
    },
    responseMiddleware
  )
  .delete((req, res, next) => {
    try {
      const fighterId = req.params.id;
      let data = FighterService.deleteFighter(fighterId);
      res.status(200).json(data[0]);
    } catch (err) {
      res.err = err;
    }

    next();
  });

module.exports = router;
